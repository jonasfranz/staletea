// staletea
// Copyright (C) 2019 Jonas Franz
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package auth

import (
	"fmt"
	"gitea.com/jonasfranz/staletea/config"
	"golang.org/x/oauth2"
)

func endpoint() oauth2.Endpoint {
	// Endpoint is Google's OAuth 2.0 endpoint.
	return oauth2.Endpoint{
		AuthURL:   fmt.Sprintf("%s/login/oauth/authorize", config.GiteaURL.Get().(string)),
		TokenURL:  fmt.Sprintf("%s/login/oauth/access_token", config.GiteaURL.Get().(string)),
		AuthStyle: oauth2.AuthStyleInParams,
	}
}

// Config returns an oauth2 config
func Config() *oauth2.Config {
	return &oauth2.Config{
		ClientID:     config.GiteaClientID.Get().(string),
		ClientSecret: config.GiteaClientSecret.Get().(string),
		Endpoint:     endpoint(),
		RedirectURL:  fmt.Sprintf("%s/callback", config.BaseURL.Get().(string)),
	}
}
