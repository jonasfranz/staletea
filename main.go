// staletea
// Copyright (C) 2019 Jonas Franz
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"gitea.com/jonasfranz/staletea/cmd"
	"gitea.com/jonasfranz/staletea/config"
	"gitea.com/jonasfranz/staletea/models"
	"github.com/urfave/cli"
	"os"
)

func main() {
	if err := config.SetupConfig(); err != nil {
		panic(err)
	}

	models.SetupDatabase()

	app := cli.NewApp()
	app.Commands = []cli.Command{cmd.CmdRun}
	if err := app.Run(os.Args); err != nil {
		panic(err)
	}
}
