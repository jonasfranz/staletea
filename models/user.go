// staletea
// Copyright (C) 2019 Jonas Franz
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package models

import (
	"code.gitea.io/sdk/gitea"
	"context"
	"gitea.com/jonasfranz/staletea/auth"
	"gitea.com/jonasfranz/staletea/config"
	"golang.org/x/oauth2"
)

// User represents a signed in oauth2 gitea user saved in a session
type User struct {
	ID       int64
	Username string
	Token    *oauth2.Token
}

// GiteaClient will return a gitea client with authentication of the user
func (user *User) GiteaClient() *gitea.Client {
	client := gitea.NewClient(config.GiteaURL.Get().(string), "")
	client.SetHTTPClient(auth.Config().Client(context.Background(), user.Token))
	return client
}
